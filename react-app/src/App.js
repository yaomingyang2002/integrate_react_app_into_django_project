import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
        <div class="center-column">
	        <div class="item-row">
	        		<span>one</span>
        	</div>
        	<div class="item-row">
	        		<span>two</span>
        	</div>
        	<div class="item-row">
	        		<span>three</span>
        	</div>
        </div>
    </div>
  );
}

export default App;
